#!/bin/bash -l

set -e

usage="
$(basename "$0") [-h]

$(basename "$0") analyze PROJECT_PATH

$(basename "$0") test PROJECT_NAME PROJECT_REF

where:
  -h  show this help text
  PROJECT_PATH = the path of the project to be analyzed for licenses usage.

  Set SETUP_CMD to skip auto-detection and use the specified command to install project dependencies."

if [ $# -eq 0 ] ; then
  echo "$usage"
  exit 1
fi

while getopts 'h' option; do
  case "$option" in
    h) echo "$usage"
       exit
       ;;
    :) printf "missing argument for -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit 1
       ;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit 1
       ;;
  esac
done
shift $((OPTIND - 1))

COMMAND=$1
shift

# "/run.sh" as a command means the user want the "analyze" command.
if [ "$COMMAND" = "/run.sh" ] ; then
  COMMAND="analyze"
fi
if [ "$COMMAND" = "analyse" ] ; then
  COMMAND="analyze"
fi

# "/test/test.sh" as a command means the user want the "test" command.
if [ "$COMMAND" = "/test/test.sh" ] ; then
  COMMAND="test"
fi

# Check number of arguments

if [ "$COMMAND" = "analyze" -a $# -ne 1 ] ; then
  echo "$usage"
  exit 1
fi

if [ "$COMMAND" = "test" -a $# -ne 2 ] ; then
  echo "$usage"
  exit 1
fi

# Run command
case "$COMMAND" in
  test)
    # Run integration tests.
    exec /test/test.sh $1 $2
    ;;

  analyze)
    # Analyze project

    # Load RVM
    source /rvm.sh

    # Change current directory to the project path.
    APP_PATH=$1
    shift
    pushd $APP_PATH > /dev/null

# adding python 3 support
    if test -f requirements.txt; then
      if test ${PYTHON_VERSION:-3} = "3"; then
        $(sudo apt install -y python3 pip3)
        ln -sf $(which python3) $(which python)
        ln -sf $(which pip3) $(which pip)git
      fi
    fi

    if [[ -z "${SETUP_CMD}" ]]; then
        # Before running license_finder, we need to install dependencies for the project.
        if test -f Gemfile ; then
          if test -n "$rvm_recommended_ruby" ; then
            # Install the Ruby version RVM found in the project files
            # This always end in the cryptic "bash: Searching: command not found" error but Ruby is installed
            # So we ignore the error.
            $($rvm_recommended_ruby) 2>/dev/null || true
            rvm use .
            gem install bundler
            # We need to install the license_finder gem into this Ruby version too.
            gem install license_finder -v "$LICENSE_FINDER_VERSION"
          fi

          # Ignore test and development dependencies.
          license_finder ignored_groups add development
          license_finder ignored_groups add test
          bundle install --without "development test"
          skip_prepare=true
        fi

        if test -f package.json; then
          # Check is npm is being used, if so convert it to yarn
          if [ -f package-lock.json ] && [ ! -f yarn.lock ] ; then
            # restore original lock file state on EXIT
            function restore_lockfile {
              echo "Cleanup generated $APP_PATH/yarn.lock"
              rm -f $APP_PATH/yarn.lock || true
            }
            trap restore_lockfile EXIT

            echo "Convert package-lock.json to yarn.lock"
            yarn import
          fi

          # install via yarn
          if [ ! -d node_modules ]; then
            yarn install --ignore-scripts

            # Try to install Peer packages too, npm install doesn't do it anymore.
            /node_modules/.bin/npm-install-peers
          fi
          skip_prepare=true
        fi


        if find . -name "*.go" -printf "found" -quit |grep found >/dev/null ; then
          if [ ! -f glide.lock -a ! -f vendor/manifest -a ! -f Gopkg.lock -a ! -f go.mod ]; then
            echo "running go get"
            # Only install deps if not using glide, govendor or dep
            # Symlink the project into GOPATH to allow fetching dependencies.
            ln -sf `realpath $APP_PATH` /gopath/src/app
            pushd /gopath/src/app > /dev/null
            go get || true
            skip_prepare=true
          fi
        fi

        if test -f pom.xml ; then
          # Install Java Maven dependencies.
          if [[ ${LM_JAVA_VERSION} = "11" ]]; then
            JAVA_HOME=/usr/lib/jvm/adoptopen_jdk11
          elif [[ ${LM_JAVA_VERSION} = "8" ]]; then
            JAVA_HOME=/usr/lib/jvm/oracle_jdk8
          fi
          mvn install ${MAVEN_CLI_OPTS:--DskipTests}
          skip_prepare=true
        fi

        if test -f build.gradle ; then
          if [[ ${LM_JAVA_VERSION} = "11" ]]; then
            JAVA_HOME=/usr/lib/jvm/adoptopen_jdk11
          elif [[ ${LM_JAVA_VERSION} = "8" ]]; then
            JAVA_HOME=/usr/lib/jvm/oracle_jdk8
          fi
          gradle build
          skip_prepare=true
        fi
    else
        echo "Running '${SETUP_CMD[@]}' to install project dependencies..."
        ${SETUP_CMD[@]}
        skip_prepare=true
    fi

    # Run License Finder.
    echo "Running license_finder $@ in $PWD"

    if [ "$skip_prepare" != true ]; then
        prepare="--prepare"
    fi
    license_finder report ${prepare} --format=html --save=gl-license-management-report.html
    # rvm removes trap in bash: https://github.com/rvm/rvm/issues/4416
    declare -f restore_lockfile > /dev/null && restore_lockfile
    popd > /dev/null

    # Extract data from the HTML report and put it into a JSON file
    node /html2json.js $APP_PATH/gl-license-management-report.html > $APP_PATH/gl-license-management-report.json
    ;;

  *)
    # Unknown command
    echo "Unknown command: $COMMAND"
    echo "$usage"
    exit 1
esac
